package com.example.piwo.controller;

import com.example.piwo.entity.Beer;
import com.example.piwo.service.BeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class BeerController {

    private final BeerService beerService;

    @Autowired
    public BeerController(BeerService beerService) {
        this.beerService = beerService;
    }

    @GetMapping("/")
    public String index() {
        return "redirect:/login";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/listBeers")
    @ResponseBody
    public Iterable<Beer> list() {
        return beerService.getAll();
    }

    @GetMapping("/addBeer")
    public String addBeer() {
        return "addBeer";
    }


    @PostMapping(path = "/addBeer")
    @ResponseBody
    public String add(@RequestParam("name") String name,
                      @RequestParam("tagline") String tagline,
                      @RequestParam("firstBrewed") String firstBrewed,
                      @RequestParam("description") String description,
                      @RequestParam("imageUrl") String imageUrl,
                      @RequestParam("ibu") int ibu,
                      @RequestParam("foodPairing1") String foodPairing1,
                      @RequestParam("foodPairing2") String foodPairing2,
                      @RequestParam("foodPairing3") String foodPairing3) {

        Beer newBeer = new Beer();
        newBeer.setName(name);
        newBeer.setTagline(tagline);
        newBeer.setFirst_brewed(firstBrewed);
        newBeer.setDescription(description);
        newBeer.setImage_url(imageUrl);
        newBeer.setIbu(ibu);
        newBeer.setFoodPairing1(foodPairing1);
        newBeer.setFoodPairing2(foodPairing2);
        newBeer.setFoodPairing3(foodPairing3);
        newBeer.setExternalApi(false);
        beerService.save(newBeer);
        return "Beer successfully added.";
    }

    @GetMapping(path = "/foodpairings/search/{phrase}", produces = "application/json")
    @ResponseBody
    public Iterable<Beer> getBeerByFoodPairings(@PathVariable("phrase") String phrase) {

        return beerService.getBeerByFoodPairings(phrase);
    }

}
