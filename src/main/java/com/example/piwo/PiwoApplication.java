package com.example.piwo;

import lombok.extern.slf4j.Slf4j;
import org.h2.tools.Server;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@Slf4j
@SpringBootApplication
@EnableScheduling
public class PiwoApplication {

    public static void main(String[] args) throws Exception {

        // start a TCP server
        // (either before or after opening the database)
        Server server = Server.createTcpServer().start();

        // .. use in embedded mode ..
        // or use it from another process:
        log.info("Server started and connection is open.");
        log.info("URL: jdbc:h2:" + server.getURL() + "/mem:test");

        // now start the H2 Console here or in another process using
        // java org.h2.tools.Console -web -browser

        SpringApplication.run(PiwoApplication.class, args);

        log.info("Press [Enter] to stop.");
        System.in.read();

        log.info("Stopping server and closing the connection");
        server.stop();

    }
}
