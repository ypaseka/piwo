package com.example.piwo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Beer {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int id;

    private String name;

    private String tagline;

    private String first_brewed;

    @Column(length = 1000)
    private String description;

    private String image_url;

    private int ibu;

    @Transient
    private List<String> food_pairing;

    private String foodPairing1;
    private String foodPairing2;
    private String foodPairing3;

    private boolean externalApi;
}
