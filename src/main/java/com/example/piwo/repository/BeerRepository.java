package com.example.piwo.repository;

import com.example.piwo.entity.Beer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BeerRepository extends CrudRepository<Beer, Integer> {

    List<Beer> findAllByFoodPairing1ContainingOrFoodPairing2ContainingOrFoodPairing3Containing(String foodPairing1, String foodPairing2, String foodPairing3);

    List<Beer> findAll();

    void deleteAllByExternalApiIsTrue();
}
