package com.example.piwo.service;

import com.example.piwo.entity.Beer;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class DownloadBeers {

    private BeerService beerService;

    public DownloadBeers(BeerService beerService) {
        this.beerService = beerService;
    }

    @Scheduled(fixedRate = 10000)//1 hour//3600000
    public void download() {

        beerService.deleteAllFromExternalApi();

        // czytaj JSON-a i zapisz do listy
        RestTemplate restTemplate = new RestTemplate();
        try {

            //Pobieramy listę piw z punkapi i zapisujemy je jako tablicę obiektów

            Beer[] beer1 = restTemplate.getForObject("https://api.punkapi.com/v2/beers?page=1&per_page=30", Beer[].class);

            //zapisujemy do listy
            List<Beer> listBeers = new ArrayList<>();
            Collections.addAll(listBeers, beer1);

            //parsujemy tablicę Jsona
            for (int i = 0; i < 30; i++) {
                listBeers.get(i).setFoodPairing1(listBeers.get(i).getFood_pairing().get(0));
                listBeers.get(i).setFoodPairing2(listBeers.get(i).getFood_pairing().get(1));
                listBeers.get(i).setFoodPairing3(listBeers.get(i).getFood_pairing().get(2));
                listBeers.get(i).setExternalApi(true);
            }
            //zapis listy piw do bazy danych H2
            beerService.saveAll(listBeers);

            System.out.println("Zapisałem listę");
        } catch (Exception e) {
            System.out.println("Błąd zapisu listy!: " + e.getMessage());
        }
    }
}
