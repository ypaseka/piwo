package com.example.piwo.service;

import com.example.piwo.entity.Beer;
import com.example.piwo.repository.BeerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("beerService")
public class BeerService {

    private final BeerRepository beerRepository;

    @Autowired
    public BeerService(BeerRepository beerRepository) {
        this.beerRepository = beerRepository;
    }

    public List<Beer> getBeerByFoodPairings(String phrase) {
        return beerRepository.findAllByFoodPairing1ContainingOrFoodPairing2ContainingOrFoodPairing3Containing(phrase, phrase, phrase);
    }

    public Beer save(Beer beer) {
        return beerRepository.save(beer);
    }

    void saveAll(List<Beer> beers) {
        beerRepository.saveAll(beers);
    }

    public Iterable<Beer> getAll() {
        return beerRepository.findAll();
    }

    String deleteAllFromExternalApi(){
        beerRepository.deleteAllByExternalApiIsTrue();
        return "Delete previos data in bd";
    }

}
